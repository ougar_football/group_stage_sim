<?php
 
require("kdb.inc");

$dbh=MyDatabase::connect("homepage");

$simulations=(int)@$_GET['simulations'];
if ($simulations<1) die("error\n");

// Update simulation count
$q="update europe_simulations set num=num+$simulations";
$dbh->kquery($q);

// Get new simulation count
$q="select format(num,0) as num from europe_simulations";
$num=$dbh->get_single_value($q);

print("$num\n");

?>
