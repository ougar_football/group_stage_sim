Group Stage Simulator
=====================

Homepage: http://www.fodbold.ougar.dk/group_stage_sim/

Show current Champions League and Europa League groups and let user estimate the
chances of each club based on probabilities for the different outcomes in the 
remaining matches.

NOTE: Needs both my php libraries and the data from the database to work.
