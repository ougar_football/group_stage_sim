<?php

require_once("kdb.inc");
require_once("docheader.inc");
require_once("simclasses.inc");
require_once("baseclass.inc");
require_once("utf8sprintf.inc");
require_once("getvar.inc.php");
require_once("season.inc.php");

$myseason=new Season();
$season=$myseason->season;
$seasonstr=$myseason->aar();

$header=new DocHeader();
$header->set_html5();
$header->set_title("Group Stage Simulation $seasonstr");
$header->add_jquery3();
$header->add_javascript("http://www.ougar.dk/javascript/jquery-ui.min.js");
$header->add_javascript("http://www.ougar.dk/javascript/MatchProbability.js");
$header->add_javascript("groupinfo.js.php?season=$season");
$header->add_javascript("groupsim.js");
$header->add_css("http://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css");
$header->add_css("style.css");
$header->add_css("slider.css");

$custom="  <meta name=\"twitter:card\" content=\"summary\" />\n".
        "  <meta name=\"twitter:site\" content=\"@kghougaard\" />\n".
        "  <meta name=\"twitter:title\" content=\"European Group Stage Simulation\" />\n".
        "  <meta name=\"twitter:description\" content=\"How will the Champions League and Europa League groups end? Who will progress to the next round?\" />\n".
        "  <meta name=\"twitter:image\" content=\"http://fodbold.ougar.dk/group_stage_sim/img/group_sim_thumb.png\" />";
$header->set_custom($custom);

$dbh=MyDatabase::connect("europacup");

$tours=getTournaments($season);
$groups=getAllGroups($season);

$header->display();
print("<body>\n".
      "<div id='header'>\n".
      "  <h1> <img src='img/championsleague.jpg'> European Group Stage Simulation $seasonstr <img src='img/europaleague.jpg'></h1>\n".
      "</div>\n");
print("<div id='tabs'>\n".
      "  <ul>\n");
foreach ($tours as $code => $tour)
  print("    <li><a href='#tab-$code'>$tour</a></li>\n");
print("  </ul>\n");
foreach (array_keys($tours) as $tour) {
  print("\n");
  print("<!-- TOURNAMENT: ".($tours[$tour])." -->\n");
  print("<div id='tab-$tour'>\n");
  $groupnum=0;
  foreach ($groups as $key=>$g) {
    if ($g->tournament==strtoupper($tour)) {
      if ($groupnum && $groupnum%2==0) print("  <div class='space'> </div>\n");
      $tourtype=getTourFormat($tour,$season);
      $groupnum++;
      print("  <table class='table'>\n".
            "    <colgroup>\n".
            "      <col class='clubname'>\n".
            "      <col class='country'>\n".
            "      <col><col><col><col>\n".
            "    </colgroup>\n".
            "    ".$g->th());
      foreach ($g->teams as $t)
        print("    ".$t->tr());
      print("  </table>\n");
    }
  }
  print("</div>\n");
}
print("\n<!-- Group calculation tab -->\n".
      "<div id='tab-calc'>\n");
# INCLUDE html from newtab #
require("newtab.html");
############################
print("</div>\n".
      "<div id='bottomclear'></div>\n".
      "</div>\n".
      "<div id='helpdialog'></div>\n".
      "<div id='footer'>\n");
$simulations=$dbh->get_single_value("select format(num,0) as num from homepage.europe_simulations");
print("  Created October 2016 - A total of <span id='simcount'>$simulations</span> group simulations made<br>\n".
      "  For comments or suggestions contact me on fodbold@ougar.dk or find me on Twitter &nbsp;".
      "<a href='https://twitter.com/kghougaard'><img src='http://ougar.dk/graphics/twitter_kghougaard.png'></a>\n".
      "</body>\n".
      "</html>\n");

?>
