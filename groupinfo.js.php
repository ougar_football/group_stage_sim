<?php

require_once("kdb.inc");
require_once("season.inc.php");

$dbh=MyDatabase::connect("europacup");
$myseason=new Season();
$season=$myseason->season;
$seasonstr=$myseason->aar();

$q="select distinct code,name from tournaments join groupres on tournament=code and season=$season";
$res=$dbh->kquery($q);
$tours=array();
while($t=$res->fetch_assoc()) $tours[]=$t;
$jsontour=json_encode($tours);

// Fetch all matches
$q="select id,tournament as t,if(isnull(groupround),NULL,groupround) as r,groupletter as l,tid,oid,gf1 as gf, ga1 as ga from matchtarget where season=$season and place1='home' and part='group' union select id,tournament as t,if(isnull(groupround),NULL,if(groupround=3,4,if(season>2021,7-groupround,4+groupround))) as r,groupletter as l, tid,oid,gf2 as gf, ga2 as ga from matchtarget where season=$season and place1='away' and part='group' order by t,l,r";
$res=$dbh->kquery($q);
$matches=array();
while ($match=$res->fetch_assoc()) {
  if (!array_key_exists($match['t'],$matches))
    $matches[$match['t']]=array();
  if (!array_key_exists($match['l'],$matches[$match['t']]))
    $matches[$match['t']][$match['l']]=array();
  $matches[$match['t']][$match['l']][]=$match;
}
$jsonmatch=json_encode($matches);

// Make list of all teams with team info and current group standings;
$q="select a.tournament as t,groupletter as g,a.team as id,if(b.name='Borussia Mönchengladbach',\"M'gladbach\",b.name) as n,b.country as c,pot,matches as m,win as w,draw as d,lose as l,points as p,goalf as f,goala as a,pos,format(c.coef,1) as tc,format(d.coef,1) as cc from groupres as a join teams as b on a.season=$season and a.team=b.id join teamcoef as c on a.team=c.team and a.season=c.season join countryrank as d on b.country=d.country and a.season=d.season order by t,g,p";
$res=$dbh->kquery($q);
$teams=array();
while ($t=$res->fetch_assoc()) {
  if (!array_key_exists($t['t'],$teams))
    $teams[$t['t']]=array();
  if (!array_key_exists($t['g'],$teams[$t['t']]))
    $teams[$t['t']][$t['g']]=array();
  $teams[$t['t']][$t['g']][$t['id']]=$t;
}
$jsonteam=json_encode($teams);

print("// This file has data for the European cups $seasonstr\n".
      "// -------------------------------------------------------------------------------------\n".
      "// It consists of 3 objects. Tournaments, Teams and Matches\n".
      "//\n".
      "// Teams:\n".
      "// Outer object has two properties 'CL' and 'EL'\n".
      "// Next level has 8 (CL) or 12 (EL) groupletters as indices.\n".
      "// Next level has 4 entries, one for each team, team id is index.\n".
      "// So to get info of Basel: Teams.CL.A[\"194\"]\n".
      "// Since I work on 1 group at a time, and get the team ids from the matchlist\n".
      "// this structure makes it easy to get info for a given team.\n".
      "// Innermost object has all team info:\n".
      "// t: tournament, g: groupletter, id: team id, n: team name, c: team country abbr,\n".
      "// pot: seeding pot, m: num matches, w: won, d: drawn, l: lost, p: points,\n".
      "// f: goals for, a: goals against, pos: current group position,".
      "// tc: team uefa coefficient, cc: country uefa coefficient\n".
      "//\n".
      "// Matches:\n".
      "// All match info from this years group stage in EL and CL, organized in the same\n".
      "// way as above, with tournament and group number as first indices.\n".
      "// Last level is info about the 12 matches, indexed from 0 to 11. Available fields:\n".
      "// id: matchid, t: tournament, r: round (1-6), l: groupletter, tid: id of hometeam,\n".
      "// oid: id of awayteam, gf: home goals (unplayed=null), ga: awaygoals (unplayed=null)\n\n");
print("var Tournaments=$jsontour\n\n");
print("var Teams=$jsonteam\n\n");
print("var Matches=$jsonmatch\n");
?>
